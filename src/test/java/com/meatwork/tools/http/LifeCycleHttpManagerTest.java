package com.meatwork.tools.http;

import com.sun.net.httpserver.HttpExchange;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@ExtendWith(MockitoExtension.class)
class LifeCycleHttpManagerTest {

	@Mock
	HttpExchange exchange;

	@BeforeEach
	void setup() {
		HttpManager.register("/path/[paul]/truc", "GET", request -> HttpResponse.ok());
		HttpManager.register("/path/normal", "GET", request -> HttpResponse.ok());

	}

	@Test
	void get_givenParams() throws URISyntaxException, IOException {
		Mockito.when(exchange.getRequestURI()).thenReturn(new URI("/path/toto/truc"));
		Mockito.when(exchange.getRequestMethod()).thenReturn("get");
		HttpRequest httpRequest = new HttpRequest(exchange);
		HttpMethod httpMethod = LifeCycleHttpManager.get(httpRequest);
		assertNotNull(httpMethod);
		assertNotNull(httpRequest.getParams());
		assertEquals("toto", httpRequest.getParams("paul"));
	}

	@Test
	void get_givenNothing() throws IOException, URISyntaxException {
		Mockito.when(exchange.getRequestURI()).thenReturn(new URI("/path/normal"));
		Mockito.when(exchange.getRequestMethod()).thenReturn("get");
		HttpRequest httpRequest = new HttpRequest(exchange);
		HttpMethod httpMethod = LifeCycleHttpManager.get(httpRequest);
		assertNotNull(httpMethod);
		assertNotNull(httpRequest.getParams());
	}
}