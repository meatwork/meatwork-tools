package com.meatwork.tools.model.impl;

import com.meatwork.tools.api.model.Validator;
import com.meatwork.tools.model.DataType;
import com.meatwork.tools.model.DatabaseException;
import com.meatwork.tools.model.contraint.Constraint;
import com.meatwork.tools.model.contraint.ConstraintClass;
import com.meatwork.tools.service.ServiceManager;
import lombok.extern.slf4j.Slf4j;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@Slf4j
public class DefaultValidator implements Validator {

    @Override
    public Set<Class<? extends Enum<?>>> getAcceptType() {
        return null;
    }

    @Override
    public boolean isValid(Map<String, Object> propertyValues, Map<String, DataType> properties) {
        List<Constraint> constraintList = ServiceManager.getList(Constraint.class);

        for (Map.Entry<String, Object> entry : propertyValues.entrySet()) {
            if (!properties.containsKey(entry.getKey())) {
                throw new DatabaseException("cannot find properties %s".formatted(entry.getKey()));
            }

            Set<Annotation> annoConstaintes = Arrays
                .stream(properties.get(entry.getKey()).annotations())
                .filter(it -> it.getClass().isAnnotationPresent(ConstraintClass.class))
                .collect(Collectors.toSet());

            for (Annotation annoConstainte : annoConstaintes) {
                Constraint constraint = constraintList
                    .stream()
                    .filter(it -> it.getClass().getName().equals(annoConstainte.getClass().getName()))
                    .findFirst()
                    .orElseThrow(() ->
                        new IllegalArgumentException(
                            "Cannot found constraint %s".formatted(annoConstainte.getClass().getName())
                        )
                    );
                boolean valid = constraint.isValid(entry.getValue(), annoConstainte);
                if (!valid) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public Map<String, String> getErrors() {
        return null;
    }
}
