package com.meatwork.tools.model.contraint;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public interface Constraint {

	boolean isValid(Object value, Object annotation);

}
