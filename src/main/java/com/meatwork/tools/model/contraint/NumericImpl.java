package com.meatwork.tools.model.contraint;

import com.meatwork.tools.model.vaildation.Numeric;
import com.meatwork.tools.service.Service;

import java.math.BigDecimal;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@Service(interfaces = Constraint.class)
public class NumericImpl implements Constraint {

    @Override
    public boolean isValid(Object value, Object annotation) {
        Numeric anno = (Numeric) annotation;
        BigDecimal bigDecimal;
        switch (value) {
            case BigDecimal element -> bigDecimal = element;
            case Double element -> bigDecimal = BigDecimal.valueOf(element);
            case Float element -> bigDecimal = new BigDecimal(element);
            case null, default -> {
                return false;
            }
        }

        return bigDecimal.precision() == anno.precision() && bigDecimal.scale() == anno.virguleNumber();
    }
}
