package com.meatwork.tools.model.contraint;

import com.meatwork.tools.service.Service;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@Service(interfaces = Constraint.class)
public class MandatoryImpl implements Constraint {

	@Override
	public boolean isValid(Object value, Object annotation) {
		return value != null;
	}
}
