package com.meatwork.tools.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
final class LifeCycleEntity {

	private final static Map<String, Supplier<? extends AbstractEntity<?>>> map = new HashMap<>();

	private LifeCycleEntity() {}

	static Collection<Supplier<? extends AbstractEntity<?>>> getList() {
		return map.values();
	}

	static void register(EntityRegister entityRegister) {
		map.put(entityRegister.getKey(), entityRegister.getEntityObject());
	}

	@SuppressWarnings("unchecked")
	static <S extends AbstractEntity<?>> Supplier<S> get(Class<?> name) {
		return (Supplier<S>) map.get(name.getName().replaceAll("\\.", "/"));
	}

}
