package com.meatwork.tools.model;

import java.util.Collection;
import java.util.function.Supplier;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public final class EntityManager {

	private EntityManager() {}

	public static void register(Class<? extends Enum<?>> entity, Supplier<? extends AbstractEntity<?>> entityObject) {
		LifeCycleEntity.register(new EntityRegister(entity, entityObject));
	}

	public static <S extends AbstractEntity<?>> Supplier<S> get(Class<?> entityName) {
		return LifeCycleEntity.get(entityName);
	}

	public static Collection<Supplier<? extends AbstractEntity<?>>> getList() {
		return LifeCycleEntity.getList();
	}

}
