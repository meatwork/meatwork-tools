package com.meatwork.tools.model;

import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class StoreMemory {

    private static final Map<IdKey, AbstractEntity<?>> store = new ConcurrentHashMap<>();

    private StoreMemory() {}

    public static void store(AbstractEntity<?> entity) {
        store.put(
            new IdKey(entity.getType().getName(), entity.entityDelegate.get(entity.entityDelegate.getFieldId())),
            entity
        );
    }

    @SuppressWarnings("unchecked")
    public static <V extends Enum<V> & Property, S extends AbstractEntity<V>> S get(String entityName, Object id) {
        return (S) store.get(new IdKey(entityName, id));
    }

    @SuppressWarnings("unchecked")
    public static <V extends Enum<V> & Property, S extends AbstractEntity<V>> List<S> getAll(Class<V> clazz) {
        return (List<S>) store.entrySet()
                              .stream()
                              .filter(it -> it.getKey().getEntityName().equals(clazz.getName()))
                              .map(Map.Entry::getValue)
                              .toList();
    }

    public static void remove(String entityName, Object id) {
        store.remove(new IdKey(entityName, id));
    }

    @Data
    static class IdKey {
        private final String entityName;
        private final Object id;
    }
}
