package com.meatwork.tools.model;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public final class SqlInsertBuilder<S extends Enum<S> & Property> {

    private final Set<String> selectColumns;
    private final String table;
    @Getter
    private String sql;
    @Getter
    private final List<SqlParameter> sqlParameters = new ArrayList<>();

    public SqlInsertBuilder(EntityDelegate<S> entityDelegate) {
        this.table = SqlTools.getTableName(entityDelegate.getType());
        this.selectColumns =
            entityDelegate
                .getChangeSet()
                .getChanges()
                .stream()
                .map(it -> {
                    String field = it.key();
                    Object value = it.newValue();
                    sqlParameters.add(new SqlParameter(field, value));
                    return field;
                })
                .collect(Collectors.toSet());
    }

    public void generate() {
        sql = (
            "INSERT INTO " +
            table +
            "(" +
            String.join(", ", selectColumns) +
            ") VALUES (" +
            selectColumns.stream().map(":%s"::formatted).collect(Collectors.joining(", ")) +
            ")"
        );
    }
}
