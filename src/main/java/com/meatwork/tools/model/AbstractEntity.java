package com.meatwork.tools.model;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class AbstractEntity<S extends Enum<S> & Property> implements Entity<S> {

    protected EntityDelegate<S> entityDelegate;

    public AbstractEntity(EntityDelegate<S> entityDelegate) {
        this.entityDelegate = entityDelegate;
    }

    @Override
    public Class<S> getType() {
        return entityDelegate.getType();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
