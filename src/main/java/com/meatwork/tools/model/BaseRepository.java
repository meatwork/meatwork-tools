package com.meatwork.tools.model;

import com.meatwork.tools.api.model.DatabaseConfiguration;
import com.meatwork.tools.api.model.ModelIdentity;
import com.meatwork.tools.api.model.Validator;
import com.meatwork.tools.event.BroadcastEvent;
import com.meatwork.tools.model.event.EntityOnSave;
import com.meatwork.tools.service.ServiceManager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class BaseRepository<V extends Enum<V> & Property, S extends AbstractEntity<V>> implements Repository<V, S> {

    private final Supplier<S> newElement;
    private final boolean storeInMemory;
    private final Class<V> clazz;

    public BaseRepository(Class<V> newElement) {
        this.clazz = newElement;
        this.newElement = EntityManager.get(newElement);
        DatabaseConfiguration databaseConfiguration = ServiceManager.get(DatabaseConfiguration.class);
        if (databaseConfiguration == null) {
            throw new DatabaseException("Not implementation for databaseConfiguration founded");
        }
        this.storeInMemory = databaseConfiguration.storeInMemory();
    }

    @Override
    public S findById(Object id) {

        S entity = newElement.get();
        if (storeInMemory) {
            S entityFromMemory = StoreMemory.get(entity.getType().getName(), id);
            if (entityFromMemory != null) {
                return entityFromMemory;
            }
        }

        String sql = getQueryBuilder().where("self.ID = :ID").build();
        try (
            ResultSet resultSet = Database.executeQuery(
                sql,
                Collections.singleton(new SqlParameter(entity.entityDelegate.getFieldId(), id))
            )
        ) {
            List<S> listEntities = extractResultSet(resultSet);
            if(storeInMemory) {
                StoreMemory.store(entity);
            }
            return listEntities.stream().findFirst().orElse(null);
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public List<S> findAll() {
        List<S> findAll;
        S entity = newElement.get();
        if(storeInMemory) {
            findAll = StoreMemory.getAll(clazz);
            if(findAll == null) {
                findAll = getAllFromDatabase(entity);
            }
        } else {
            findAll = getAllFromDatabase(entity);
        }

        return findAll;
    }

    @Override
    public ValidatorFlow<V> save(S entity) {
        List<Validator> validator = ServiceManager.getList(Validator.class);

        if (
            validator == null ||
            validator
                .stream()
                .filter(it ->
                    it.getAcceptType() == null ||
                    it
                        .getAcceptType()
                        .stream()
                        .anyMatch(cls -> cls.getName().equals(entity.entityDelegate.getType().getName()))
                )
                .allMatch(it ->
                    it.isValid(entity.entityDelegate.getCopyOfEntityMap(), entity.entityDelegate.getProperties())
                )
        ) {
            if (entity.entityDelegate.get(entity.entityDelegate.getFieldId()) == null) {
                ModelIdentity modelIdentity = ServiceManager
                    .getList(ModelIdentity.class)
                    .stream()
                    .map(it -> (ModelIdentity) it)
                    .filter(it ->  it.acceptTypes() == null || it.acceptTypes().contains(entity.getType()))
                    .findFirst()
                    .orElseThrow(() ->
                        new DatabaseException(
                            "cannot generated entity for %s ModelIdentity not found".formatted(
                                    entity.getType().getName()
                                )
                        )
                    );
                Object idValue = modelIdentity.generateIdentity(entity.entityDelegate);
                entity.entityDelegate.put(entity.entityDelegate.getFieldId(), idValue);
                SqlInsertBuilder<V> sqlInsertBuilder = new SqlInsertBuilder<>(entity.entityDelegate);
                sqlInsertBuilder.generate();
                Database.executeUpdate(
                    sqlInsertBuilder.getSql(),
                    sqlInsertBuilder.getSqlParameters()
                );
            } else {
                SqlUpdateBuilder sqlUpdateBuilder = new SqlUpdateBuilder(entity.entityDelegate);
                sqlUpdateBuilder.generate();
                Object idValue = entity.entityDelegate.get(entity.entityDelegate.getFieldId());
                List<SqlParameter> listParameter = sqlUpdateBuilder.getSqlParameters();
                listParameter.add(new SqlParameter(
                        entity.entityDelegate.getFieldId(),
                        idValue
                ));
                Database.executeUpdate(
                    sqlUpdateBuilder.getSql(),
                    listParameter
                );
            }

            entity.entityDelegate.applyChange();
            if (storeInMemory) {
                StoreMemory.store(entity);
            }
            BroadcastEvent.send(new EntityOnSave(entity.entityDelegate));
            return new ValidatorFlow<>(true, null, entity.entityDelegate);
        }

        return new ValidatorFlow<>(
            false,
            validator
                .stream()
                .flatMap(it -> it.getErrors().entrySet().stream())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)),
            entity.entityDelegate
        );
    }

    @Override
    public long getNextSequence() {
        S entity = newElement.get();
        String sql =
            "SELECT self.%s FROM %s self ORDER BY %s DESC LIMIT 1".formatted(
                    entity.entityDelegate.getFieldId(),
                    SqlTools.getTableName(entity.entityDelegate.getType()),
                    entity.entityDelegate.getFieldId()
                );
        try (ResultSet resultSet = Database.executeQuery(sql)) {
            if (resultSet.getFetchSize() == 0) {
                return 1;
            } else {
                return resultSet.getLong(entity.entityDelegate.getFieldId());
            }
        } catch (Exception e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public void delete(S entity) {
        SqlDeleteBuilder<V> sqlDeleteBuilder = new SqlDeleteBuilder<>(entity.entityDelegate);
        List<SqlParameter> sqlParameters = List.of(new SqlParameter(
                entity.entityDelegate.getFieldId(),
                entity.entityDelegate.get(entity.entityDelegate.getFieldId())
        ));
        Database.executeUpdate(sqlDeleteBuilder.build(), sqlParameters);

        if (storeInMemory) {
            StoreMemory.remove(entity.getType().getName(), entity.entityDelegate.get(entity.entityDelegate.getFieldId()));
        }
    }

    @Override
    public SqlQueryBuilder<V> getQueryBuilder() {
        return new SqlQueryBuilder<>(newElement.get().entityDelegate);
    }

    private List<S> getAllFromDatabase(S entity) {
        SqlQueryBuilder<V> sqlQueryBuilder = new SqlQueryBuilder<>(entity.entityDelegate);
        String sql = sqlQueryBuilder.build();
        try(ResultSet resultSet = Database.executeQuery(sql)) {
            return extractResultSet(resultSet);
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    private List<S> extractResultSet(ResultSet resultSet) throws SQLException {
        List<S> list = new ArrayList<>();
        while (resultSet.next()) {
            S newEntity = newElement.get();
            for (Map.Entry<String, DataType> property : newEntity.entityDelegate.getProperties().entrySet()) {
                if (property
                        .getValue()
                        .type()
                        .isAssignableFrom(Collection.class)) {
                    continue;
                }

                newEntity.entityDelegate.put(property.getKey(), resultSet.getObject(property.getKey()));
            }
            newEntity.entityDelegate.applyChange();
            list.add(newEntity);
        }

        return list;
    }
}
