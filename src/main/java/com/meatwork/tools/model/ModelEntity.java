package com.meatwork.tools.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ModelEntity {

    String table() default DEFAULT.NONE;
    Class<? extends Enum<? extends Property>>[] childOf() default { };

    class DEFAULT {
        static final String NONE = "none";
    }
}
