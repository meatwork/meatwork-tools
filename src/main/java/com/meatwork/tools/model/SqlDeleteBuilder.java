package com.meatwork.tools.model;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public final class SqlDeleteBuilder<S extends Enum<S> & Property> {

    private final String table;
    private final String idField;

    public SqlDeleteBuilder(EntityDelegate<S> entityDelegate) {
        this.table = SqlTools.getTableName(entityDelegate.getType());
        this.idField = entityDelegate.getFieldId();
    }

    public String build() {
        return (
            "DELETE FROM " +
            table +
            " WHERE " +
            idField +
            " = :" +
            idField
        );
    }
}
