package com.meatwork.tools.model;

import java.util.List;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public interface Repository<V extends Enum<V> & Property, S extends AbstractEntity<V>> {

    S findById(Object id);

	List<S> findAll();

	ValidatorFlow<V> save(S entity);

	long getNextSequence();

	SqlQueryBuilder<V> getQueryBuilder();
	void delete(S entity);
}
