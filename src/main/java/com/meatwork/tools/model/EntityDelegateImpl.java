package com.meatwork.tools.model;

import java.util.HashMap;
import java.util.Map;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class EntityDelegateImpl<S extends Enum<S> & Property> implements EntityDelegate<S> {

    private final HashMap<String, Object> map = new HashMap<>();
    private final Class<S> type;
    private final Map<String, DataType> properties;
    private final String fieldId;

    ChangeSet changeSet;

    public EntityDelegateImpl(Class<S> type, String fieldId, Map<String, DataType> properties) {
        this.fieldId = fieldId;
        if (this.fieldId == null) {
            throw new DatabaseException("cannot found property type identity");
        }
        this.properties = properties;
        this.type = type;
    }

    @Override
    public Map<String, DataType> getProperties() {
        return properties;
    }

    @Override
    public Class<S> getType() {
        return type;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<String, Object> getCopyOfEntityMap() {
        return (Map<String, Object>) map.clone();
    }

    @Override
    public String getFieldId() {
        return fieldId;
    }

    @Override
    public void put(String key, Object value) {
        if (changeSet == null) {
            this.changeSet = new ChangeSet(type);
        }
        this.changeSet.addChange(new Change(key, map.get(key), value));
    }

    @Override
    public ChangeSet getChangeSet() {
        return changeSet;
    }

    @Override
    public Object get(String name) {
        return getCopyOfEntityMap().get(name);
    }

    @Override
    public void applyChange() {
        if (this.changeSet == null) {
            return;
        }

        for (Change change : this.changeSet.getChanges()) {
            this.map.put(change.key(), change.newValue());
        }
        this.changeSet = null;
    }
}
