package com.meatwork.tools.model;

import java.util.Map;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public interface EntityDelegate<S extends Enum<S> & Property> {
	Map<String, DataType> getProperties();

    Class<S> getType();

    Map<String, Object> getCopyOfEntityMap();

    String getFieldId();

    void put(String key, Object value);

	ChangeSet getChangeSet();

	Object get(String name);

	void applyChange();
}
