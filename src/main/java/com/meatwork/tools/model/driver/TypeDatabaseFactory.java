package com.meatwork.tools.model.driver;

import com.meatwork.tools.model.DatabaseException;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public final class TypeDatabaseFactory {

	private TypeDatabaseFactory() {}

	public static TypeDatabase getTypeDatabaseByDriver(String driver) {
		return switch (driver) {
			case "org.h2.Driver" -> new H2Type();
			case "org.postgresql.Driver" -> new PostgresType();
			default -> throw new DatabaseException("Database %s not supported".formatted(driver));
		};
	}

}
