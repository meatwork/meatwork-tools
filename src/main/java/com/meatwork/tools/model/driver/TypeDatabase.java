package com.meatwork.tools.model.driver;

import java.lang.annotation.Annotation;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public interface TypeDatabase {

	String valueOf(Class<?> clazz, Annotation[] annotations);

}
