package com.meatwork.tools.model.driver;

import com.meatwork.tools.model.DatabaseException;
import com.meatwork.tools.model.vaildation.Length;
import com.meatwork.tools.model.vaildation.Numeric;
import com.meatwork.tools.model.vaildation.Text;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Optional;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class PostgresType implements TypeDatabase {

    @Override
    public String valueOf(Class<?> clazz, Annotation[] annotations) {
        if (TypeDatabaseAvailable.isSameAs(clazz, TypeDatabaseAvailable.STRING)) {
            if (Arrays.stream(annotations).anyMatch(it -> it.getClass().getName().equals(Text.class.getName()))) {
                return "text";
            } else {
                Optional<Length> length = Arrays
                    .stream(annotations)
                    .filter(it -> it.getClass().getName().equals(Length.class.getName()))
                    .map(it -> (Length) it)
                    .findFirst();
                if (length.isPresent()) {
                    return "varchar(%s)".formatted(length.get().max());
                } else {
                    return "varchar(255)";
                }
            }
        } else if (TypeDatabaseAvailable.isSameAs(clazz, TypeDatabaseAvailable.INTEGER)) {
            return "integer";
        } else if (TypeDatabaseAvailable.isSameAs(clazz, TypeDatabaseAvailable.LONG)) {
            return "bigint";
        } else if (
            TypeDatabaseAvailable.isSameAs(clazz, TypeDatabaseAvailable.DOUBLE) ||
            TypeDatabaseAvailable.isSameAs(clazz, TypeDatabaseAvailable.BIGDECIMAL)
        ) {
            Optional<Numeric> numeric = Arrays
                .stream(annotations)
                .filter(it -> it.getClass().getName().equals(Length.class.getName()))
                .map(it -> (Numeric) it)
                .findFirst();
            if (numeric.isPresent()) {
                Numeric numericAnno = numeric.get();
                return "numeric(%s, %s)".formatted(numericAnno.precision(), numericAnno.virguleNumber());
            } else {
                return "numeric(10, 0)";
            }
        } else if (
            TypeDatabaseAvailable.isSameAs(clazz, TypeDatabaseAvailable.LOCALDATE) ||
            TypeDatabaseAvailable.isSameAs(clazz, TypeDatabaseAvailable.LOCALDATETIME)
        ) {
            return "timestamp";
        } else if (TypeDatabaseAvailable.isSameAs(clazz, TypeDatabaseAvailable.BOOLEAN)) {
            return "boolean";
        } else {
            throw new DatabaseException(
                "type converter for class %s for postgresl not supported".formatted(clazz.getName())
            );
        }
    }
}
