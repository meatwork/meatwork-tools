package com.meatwork.tools.model.driver;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class TypeDatabaseAvailable {

    public static final String STRING = String.class.getName();
    public static final String INTEGER = Integer.class.getName();
    public static final String LONG = Long.class.getName();
    public static final String DOUBLE = Double.class.getName();
    public static final String BIGDECIMAL = BigDecimal.class.getName();
    public static final String LOCALDATE = LocalDate.class.getName();
    public static final String LOCALDATETIME = LocalDateTime.class.getName();
    public static final String BOOLEAN = Boolean.class.getName();

    private TypeDatabaseAvailable() {
        //private constructor
    }

    public static boolean isSameAs(Class<?> clazz, String comparator) {
        return clazz.getName().equals(comparator);
    }

    public static boolean isNotPrimitifType(Class<?> clazz) {
        return !List
            .of(STRING, INTEGER, LONG, DOUBLE, BIGDECIMAL, LOCALDATE, LOCALDATETIME, BOOLEAN)
            .contains(clazz.getName());
    }
}
