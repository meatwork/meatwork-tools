package com.meatwork.tools.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */

@Getter
@RequiredArgsConstructor
public class ChangeSet {

	public final List<Change> changes = new ArrayList<>();
	public final Class<? extends Enum<?>> entity;

	public void addChange(Change change) {
		this.changes.add(change);
	}

}
