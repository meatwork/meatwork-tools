package com.meatwork.tools.model.vaildation;

import com.meatwork.tools.model.contraint.ConstraintClass;
import com.meatwork.tools.model.contraint.NumericImpl;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@ConstraintClass(NumericImpl.class)
public @interface Numeric {

	int precision() default 10;
	int virguleNumber() default 0;

}
