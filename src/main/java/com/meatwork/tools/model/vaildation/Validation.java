package com.meatwork.tools.model.vaildation;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public interface Validation {

	String getTypeOfDatabase();

}
