package com.meatwork.tools.model.vaildation;

import com.meatwork.tools.model.contraint.ConstraintClass;
import com.meatwork.tools.model.contraint.LengthImpl;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@ConstraintClass(LengthImpl.class)
public @interface Length {
	int min() default -1;
	int max() default  -1;
}
