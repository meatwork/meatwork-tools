package com.meatwork.tools.model;

import java.lang.annotation.Annotation;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public record DataType(Class<?> type, Annotation[] annotations) {
}
