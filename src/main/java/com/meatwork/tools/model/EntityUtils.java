package com.meatwork.tools.model;

import java.util.Arrays;
import java.util.Optional;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
final class EntityUtils {

    public static String getIdName(Class<? extends Enum<?>> entity) {
        Optional<? extends Enum<?>> idField = Arrays
            .stream(entity.getEnumConstants())
            .filter(it -> {
                try {
                    return it.getClass().getField(it.name()).isAnnotationPresent(Id.class);
                } catch (NoSuchFieldException e) {
                    throw new RuntimeException(e);
                }
            })
            .findFirst();
        if (idField.isPresent()) {
            return idField.get().name();
        }

        ModelEntity annotation = entity.getAnnotation(ModelEntity.class);
        if (annotation != null) {
            for (Class<? extends Enum<?>> aClass : annotation.childOf()) {
                String idName = getIdName(aClass);
                if (idName != null) {
                    return idName;
                }
            }
        }

        return null;
    }
}
