package com.meatwork.tools.model.event;

import com.meatwork.tools.event.Event;
import com.meatwork.tools.model.EntityDelegate;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public record EntityOnSave(EntityDelegate<?> entityDelegate) implements Event {}
