package com.meatwork.tools.model;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public final class SqlUpdateBuilder {

    private final String table;
    private final ChangeSet changeSet;
    private final String idField;
    @Getter
    private String sql;
    @Getter
    private final List<SqlParameter> sqlParameters = new ArrayList<>();

    public SqlUpdateBuilder(EntityDelegate<?> entityDelegate) {
        this.table = SqlTools.getTableName(entityDelegate.getType());
        this.changeSet = entityDelegate.getChangeSet();
        this.idField = entityDelegate.getFieldId();
    }

    public void generate() {
        StringBuilder stringBuilder = new StringBuilder("UPDATE ").append(table).append(" SET ");
        String collect = changeSet
            .getChanges()
            .stream()
            .filter(it -> !it.oldValue().equals(it.newValue()))
            .map(it -> {
                String field = it.key();
                sqlParameters.add(new SqlParameter(
                        field,
                        it.newValue()
                ));
                return field;
            })
            .filter(it -> !it.equals(idField))
            .map(it -> "%s = :%s".formatted(it, it))
            .collect(Collectors.joining(", "));
        stringBuilder.append(collect);
        stringBuilder.append(" WHERE ").append(idField).append(" = :").append(idField);
        sql = stringBuilder.toString();
    }
}
