package com.meatwork.tools.model;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SqlTools {

    public static String getTableName(Class<?> entity) {
        ModelEntity annotation = entity.getAnnotation(ModelEntity.class);
        String tableName = annotation.table();
        if (tableName.equals(ModelEntity.DEFAULT.NONE)) {
            tableName = toSnakeCase(entity.getSimpleName());
        }
        return tableName;
    }

    private static String toSnakeCase(String tableName) {
        return tableName
            .replaceAll("([A-Z]+)([A-Z][a-z])", "$1_$2")
            .replaceAll("([a-z])([A-Z])", "$1_$2")
            .toUpperCase();
    }
}
