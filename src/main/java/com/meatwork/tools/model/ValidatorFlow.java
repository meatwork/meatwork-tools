package com.meatwork.tools.model;

import java.util.Map;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public record ValidatorFlow<S extends Enum<S> & Property>(boolean isValid, Map<String, String> errors, EntityDelegate<S> entityDelegate){
}
