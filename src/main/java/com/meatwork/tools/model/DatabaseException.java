package com.meatwork.tools.model;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class DatabaseException extends RuntimeException{

    public DatabaseException(String message) {
        super(message);
    }

    public DatabaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public DatabaseException(Throwable cause) {
        super(cause);
    }
}
