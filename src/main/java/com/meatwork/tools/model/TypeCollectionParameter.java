package com.meatwork.tools.model;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */

public final class TypeCollectionParameter<S extends Collection<?>, V> {

    public static <S extends Collection<?>, V> Class<?> of(Class<S> classCollection, Class<V> type) {
        if (classCollection.isAssignableFrom(List.class)) {
            return Stream.of().map(it -> type).toList().getClass();
        } else if (classCollection.isAssignableFrom(Set.class)) {
            return Stream.of().map(it -> type).collect(Collectors.toSet()).getClass();
        } else {
            throw new IllegalArgumentException("cannot convert collection on %s".formatted(classCollection.getName()));
        }
    }
}
