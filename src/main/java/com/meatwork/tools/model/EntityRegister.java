package com.meatwork.tools.model;

import lombok.Getter;

import java.util.function.Supplier;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@Getter
final class EntityRegister {

	private final String key;
	private final Supplier<? extends AbstractEntity<?>> entityObject;

	public EntityRegister(Class<? extends Enum<?>> clazz, Supplier<? extends AbstractEntity<?>> entityObject) {
		this.key = clazz.getName().replaceAll("\\.", "/");
		this.entityObject = entityObject;
	}
}
