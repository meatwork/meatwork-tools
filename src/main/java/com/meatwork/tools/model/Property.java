package com.meatwork.tools.model;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public interface Property {

	Class<?> getType();

}
