package com.meatwork.tools.model;

import com.meatwork.tools.model.driver.TypeDatabase;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class SqlCreateBuilder<S extends Enum<S> & Property> {

    private final String table;
    private final Map<String, DataType> properties;
    private final TypeDatabase typeDatabase;
    @Getter
    private final List<String> constraintes = new ArrayList<>();
    private final String fieldId;
    @Getter
    private String sql;

    public SqlCreateBuilder(EntityDelegate<S> entityDelegate, TypeDatabase typeDatabase) {
        this.table = SqlTools.getTableName(entityDelegate.getType());
        this.properties = entityDelegate.getProperties();
        this.typeDatabase = typeDatabase;
        this.fieldId = entityDelegate.getFieldId();
    }

    public void generate() {
        StringBuilder sb = new StringBuilder("CREATE TABLE ").append(table).append(" (");
        List<String> elements = new ArrayList<>();

        for (Map.Entry<String, DataType> entry : properties.entrySet()) {
            DataType dataType = entry.getValue();
            String fieldName = entry.getKey();
            if (dataType.type().isAssignableFrom(Collection.class)) {
                continue;
            }

            if (fieldName.equals(fieldId)) {
                elements.add("PRIMARY KEY(%s)".formatted(fieldName));
            }

            elements.add("%s %s".formatted(fieldName, typeDatabase.valueOf(dataType.type(), dataType.annotations())));
        }

        sql = sb.append(String.join(",", elements)).append(")").toString();
    }
}
