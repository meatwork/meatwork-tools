package com.meatwork.tools.model;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public record Change(String key, Object oldValue, Object newValue) {}
