package com.meatwork.tools.model;

import com.meatwork.tools.model.driver.TypeDatabaseAvailable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class SqlQueryBuilder<S extends Enum<S> & Property> {

    private final String table;
    private final Set<String> selectColumns;
    private final String idField;
    private final List<String> whereConditions = new ArrayList<>();
    private final List<String> joinConditions = new ArrayList<>();
    private String[] orderByColumn;
    private String ascendingOrder;

    public SqlQueryBuilder(EntityDelegate<S> entityDelegate) {
        selectColumns = entityDelegate.getProperties().keySet().stream().map(it -> {
            if (TypeDatabaseAvailable.isNotPrimitifType(entityDelegate.getProperties().get(it).type())) {
                return it + "_ID";
            }
            return it;
        }).collect(Collectors.toSet());
        this.table = SqlTools.getTableName(entityDelegate.getType());
        this.idField = entityDelegate.getFieldId();
    }

    public SqlQueryBuilder<S> where(String condition) {
        whereConditions.add(condition);
        return this;
    }

   /* public <E extends Enum<E> & Property> SqlQueryBuilder<S> leftJoin(Class<E> entity) {
        String tableName = SqlTools.getTableName(entity);
        String tablenameLowerCase = tableName.toLowerCase();
        joinConditions.add(
            "LEFT JOIN " +
            tableName +
            " as " +
            tablenameLowerCase +
            " ON self." +
            this.idField +
            " = " +
            tablenameLowerCase +
            "." +
            entity.getSimpleName().toUpperCase() +
            "_ID"
        );
        return this;
    }

    public <E extends Enum<E> & Property> SqlQueryBuilder<S> rightJoin(Class<E> entity) {
        String tableName = SqlTools.getTableName(entity);
        String tablenameLowerCase = tableName.toLowerCase();
        joinConditions.add(
            "RIGHT JOIN " +
            tableName +
            " as " +
            tablenameLowerCase +
            " ON self." +
            this.idField +
            " = " +
            tablenameLowerCase +
            "." +
            entity.getSimpleName().toUpperCase() +
            "_ID"
        );
        return this;
    }*/

    @SafeVarargs
    public final SqlQueryBuilder<S> orderBy(Ascending ascending, S... field) {
        orderByColumn = Arrays.stream(field).map(Enum::name).toArray(String[]::new);
        ascendingOrder = ascending.name();
        return this;
    }

    public String build() {
        StringBuilder query = new StringBuilder("SELECT ");
        query.append(String.join(", ", selectColumns));
        query.append(" FROM ").append(table).append(" self");
        if (!joinConditions.isEmpty()) {
            query.append(" ").append(String.join(" ", joinConditions));
        }

        if (!whereConditions.isEmpty()) {
            query.append(" WHERE ").append(String.join(" AND ", whereConditions));
        }

        if (orderByColumn != null) {
            query.append(" ORDER BY ").append(String.join(", ", orderByColumn));
            query.append(" ").append(ascendingOrder);
        }
        return query.toString();
    }

    public enum Ascending {
        ASC,
        DESC,
    }
}
