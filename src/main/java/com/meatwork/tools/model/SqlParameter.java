package com.meatwork.tools.model;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public record SqlParameter(String key, Object value) {
}
