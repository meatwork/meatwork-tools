package com.meatwork.tools.model;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public interface Entity<S extends Enum<S> & Property> extends Cloneable {
	Class<S> getType();

}
