package com.meatwork.tools.model;

import com.meatwork.tools.api.model.DatabaseConfiguration;
import com.meatwork.tools.model.driver.TypeDatabase;
import com.meatwork.tools.model.driver.TypeDatabaseFactory;
import com.meatwork.tools.service.ServiceManager;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@Slf4j
public final class Database {

    private static final Connection connection;
    private static final DatabaseConfiguration databaseConfiguration;
    static {
        try {
            databaseConfiguration = ServiceManager.get(DatabaseConfiguration.class);
            if (databaseConfiguration == null) {
                throw new DatabaseException("DatabaseConfiguration not implemented !");
            }

            Class.forName(databaseConfiguration.getDriver());
            connection =
                DriverManager.getConnection(
                    databaseConfiguration.geHost(),
                    databaseConfiguration.getUser(),
                    databaseConfiguration.getPassword()
                );
            log.info("connection establish");
        } catch (ClassNotFoundException | SQLException e) {
            throw new DatabaseException("cannot establish connection to database", e);
        }
    }

    public static void init() {
        Collection<AbstractEntity<?>> entities = EntityManager
            .getList()
            .stream()
            .map(Supplier::get)
            .collect(Collectors.toSet());

        List<String> constraintes = new ArrayList<>();

        for (AbstractEntity<?> entity : entities) {
            TypeDatabase typeDatabase = TypeDatabaseFactory.getTypeDatabaseByDriver(databaseConfiguration.getDriver());
            SqlCreateBuilder<?> sqlCreateBuilder = new SqlCreateBuilder<>(
                    entity.entityDelegate,
                    typeDatabase
            );
            sqlCreateBuilder.generate();
            executeUpdate(
                    sqlCreateBuilder.getSql(),
                    null
            );
            constraintes.addAll(sqlCreateBuilder.getConstraintes());
        }

        executeUpdate(String.join("\n", constraintes), null);
    }

    public static ResultSet executeQuery(final String query, Collection<SqlParameter> parameters) {
        try  {
            NamedParameterStatement namedParameterStatement = new NamedParameterStatement(
                    connection,
                    query
            );
            if (parameters != null) {
                for (SqlParameter parameter : parameters) {
                    namedParameterStatement.setObject(parameter.key(), parameter.value());
                }
            }
            return namedParameterStatement.executeQuery();
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    public static int executeUpdate(final String query, Collection<SqlParameter> parameters) {
        try {
            NamedParameterStatement namedParameterStatement = new NamedParameterStatement(
                    connection,
                    query
            );
            if (parameters != null) {
                for (SqlParameter parameter : parameters) {
                    namedParameterStatement.setObject(parameter.key(), parameter.value());
                }
            }
            return namedParameterStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    public static ResultSet executeQuery(String query) {
        return executeQuery(query, null);
    }

    private static void prepareArguments(
        final String query,
        final Collection<SqlParameter> parameters,
        final PreparedStatement preparedStatement
    ) throws SQLException {
        if (parameters == null) {
            return;
        }

        char[] charArray = query.toCharArray();
        StringBuilder word = new StringBuilder();
        boolean wordStarted = false;
        int index = 1;
        for (char c : charArray) {
            if (c == ' ' && wordStarted) {
                wordStarted = false;
                String finalWord = word.toString();
                SqlParameter sqlParameter = parameters
                    .stream()
                    .filter(it -> it.key().contentEquals(finalWord))
                    .findFirst()
                    .orElseThrow(() ->
                        new DatabaseException("cannot found parameter %s but it's declared".formatted(finalWord))
                    );
                preparedStatement.setObject(index, sqlParameter.value(), bindingType(sqlParameter.value()));
                index++;
                word = new StringBuilder();
            }

            if (wordStarted) {
                word.append(c);
            }

            if (c == ':') {
                wordStarted = true;
            }
        }
    }

    public boolean testConnection() {
        return true;
    }

    private static int bindingType(Object object) {
        String name = object.getClass().getName();
        try {
            TypeBinder typeBinder = TypeBinder.valueOf(name);
            return typeBinder.getTypes();
        } catch (IllegalArgumentException e) {
            throw new DatabaseException("type %s not supported".formatted(name));
        }
    }

    @RequiredArgsConstructor
    @Getter
    enum TypeBinder {
        String(Types.VARCHAR),
        Integer(Types.INTEGER),
        Long(Types.BIGINT),
        BigDecimal(Types.DECIMAL),
        Double(Types.DECIMAL),
        Float(Types.DECIMAL);

        private final int types;
    }

    private record Arguments(int index, Object value) {}
}
