package com.meatwork.tools.service;

import java.util.List;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class ServiceManager {

    private ServiceManager() {

    }

    @SuppressWarnings("unchecked")
    public static <T> T get(Class<T> clazz) {
        return (T) LifeCycleServiceManager.get(clazz);
    }

    @SuppressWarnings("unchecked")
    public static <T> List<T> getList(Class<?> clazz) {
        return (List<T>) LifeCycleServiceManager.getList(clazz);
    }

    public static void register(Class<?> clazz, Object instance) {
        LifeCycleServiceManager.registerService(new ServiceRegistration(clazz, instance));
    }

    public static void unregister(Class<?> clazz) {
        LifeCycleServiceManager.unregisterService(new ServiceUnregistration(clazz));
    }

}
