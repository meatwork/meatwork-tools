/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
package com.meatwork.tools.service;

/**
 * The interface Service component.
 */
public interface ServiceComponent {

    /**
     * Register service.
     */
    void registerService();

    /**
     * Register bind.
     */
    void registerBind();

}
