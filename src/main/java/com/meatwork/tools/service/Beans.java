package com.meatwork.tools.service;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class Beans {

    @SuppressWarnings("unchecked")
    public static <T> T get(Class<T> clazz) {
        return (T) ServiceManager.get(clazz);
    }
}
