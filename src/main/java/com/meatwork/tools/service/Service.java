/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
package com.meatwork.tools.service;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Service {

    Class<?> interfaces();

}
