package com.meatwork.tools.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
class LifeCycleServiceManager {

    private static final Map<String, List<Object>> serviceList = new TreeMap<>();

    private LifeCycleServiceManager() {}

    static void registerService(ServiceRegistration serviceRegistration) {
        if (serviceList.containsKey(serviceRegistration.getKey())) {
            serviceList.get(serviceRegistration.getKey()).add(serviceRegistration.getService());
        } else {
            ArrayList<Object> list = new ArrayList<>();
            list.add(serviceRegistration.getService());
            serviceList.put(serviceRegistration.getKey(), list);
        }
    }

    static void unregisterService(ServiceUnregistration serviceUnregistration) {
        serviceList.remove(serviceUnregistration.getKey());
    }

    static Object get(Class<?> clazz) {
        List<Object> list = getList(clazz);
        if (list == null) {
            return null;
        }
        return list.getFirst();
    }

    static List<Object> getList(Class<?> clazz) {
        return serviceList.get(
                clazz
                        .getName()
                        .replaceAll(
                                Pattern.quote("."),
                                "/"
                        )
        );
    }

}