/*
 * Copyright (c) 2016 Bull/Atos.
 * All rights reserved.
 */
package com.meatwork.tools.service;

import lombok.Getter;

/**
 * [Enter type description here].
 *
 * @author Taliro
 */
@Getter
public final class ServiceRegistration {

    private final String key;
    private final Object service;

    public ServiceRegistration(Class<?> clazz, Object service) {
        this.key = clazz.getName().replaceAll("\\.", "/");
        this.service = service;
    }
}
