package com.meatwork.tools.service;

import lombok.Getter;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@Getter
public class ServiceUnregistration {

	private final String key;

	public ServiceUnregistration(Class<?> clazz) {
		this.key = clazz.getName().replaceAll("\\.", "/");
	}
}
