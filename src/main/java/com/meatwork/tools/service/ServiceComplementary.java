package com.meatwork.tools.service;

import com.meatwork.tools.api.model.Validator;
import com.meatwork.tools.model.contraint.Constraint;
import com.meatwork.tools.model.contraint.LengthImpl;
import com.meatwork.tools.model.contraint.MandatoryImpl;
import com.meatwork.tools.model.contraint.NumericImpl;
import com.meatwork.tools.model.impl.DefaultValidator;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class ServiceComplementary {

	public static void addToRegistryServiceModel() {
		ServiceManager.register(Constraint.class, new LengthImpl());
		ServiceManager.register(Constraint.class, new MandatoryImpl());
		ServiceManager.register(Constraint.class, new NumericImpl());
		ServiceManager.register(Validator.class, new DefaultValidator());
	}

}
