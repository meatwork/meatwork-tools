package com.meatwork.tools.api.model;

import java.util.Map;
import java.util.Set;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public interface CustomizerAfterValidation {

	Set<Class<? extends Enum<?>>> getAcceptType();

	void customizer(Map<String, Object> property);

}
