package com.meatwork.tools.api.model;

import com.meatwork.tools.model.EntityDelegate;

import java.util.Set;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public interface ModelIdentity {

	Object generateIdentity(EntityDelegate<?> entityDelegate);

	Set<Class<? extends Enum<?>>> acceptTypes();

}
