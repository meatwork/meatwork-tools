package com.meatwork.tools.api.model;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public interface DatabaseConfiguration {

	String getUser();
	String getPassword();
	String geHost();
	String getDriver();
	boolean storeInMemory();

}
