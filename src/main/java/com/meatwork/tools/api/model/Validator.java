package com.meatwork.tools.api.model;

import com.meatwork.tools.model.DataType;

import java.util.Map;
import java.util.Set;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public interface Validator {

	Set<Class<? extends Enum<?>>> getAcceptType();

	boolean isValid(Map<String, Object> propertyValues, Map<String, DataType> properties);

	Map<String, String> getErrors();

}
