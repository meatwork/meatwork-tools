package com.meatwork.tools.api.http;

import com.meatwork.tools.http.HttpRequest;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public interface Authentication {

	boolean checkAuthentication(HttpRequest httpRequest);

}
