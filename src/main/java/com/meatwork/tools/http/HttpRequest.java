package com.meatwork.tools.http;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.common.io.CharStreams;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import lombok.Getter;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */

public final class HttpRequest {

    @Getter
    private final HttpMethodType httpMethodType;
    @Getter
    private final Headers requestHeaders;
    @Getter
    private String url;
    @Getter
    private final List<ParameterRequest> params = new ArrayList<>();

    private static final ObjectMapper mapper = new ObjectMapper();


    private final String body;

    static {
        mapper.registerModule(new JavaTimeModule());
    }

    public HttpRequest(HttpExchange exchange) throws IOException {
        this.httpMethodType = HttpMethodType
                .valueOf(
                        exchange
                                .getRequestMethod()
                                .toUpperCase());
        getUrl(exchange);
        comptuedParameters(exchange);
        this.requestHeaders = exchange.getRequestHeaders();


        if (exchange.getRequestBody() != null) {
            body = CharStreams.toString(new InputStreamReader(exchange.getRequestBody()));
        } else {
            body = null;
        }

    }

    @SuppressWarnings("unchecked")
    public <T> T getParams(String key) {
        return (T) this.params
                .stream()
                .filter(it -> it
                        .key()
                        .equals(key))
                .findFirst()
                .orElse(new ParameterRequest(null, null))
                .value();
    }


    private void getUrl(HttpExchange exchange) {
        String url = exchange
                .getRequestURI()
                .toString()
                .split("\\?")[0];

        String[] cutUrl = url.split("/");

        String[] cutParamUrl = Arrays.copyOfRange(
                cutUrl,
                1,
                cutUrl.length
        );

        this.url = "/" + String.join("/", cutParamUrl);
    }


    private void comptuedParameters(HttpExchange exchange) {
        String[] cutUrl = exchange
                .getRequestURI()
                .toString()
                .split("\\?");

        if (cutUrl.length == 1) {
            return;
        }

        String[] params = cutUrl[1]
                .split("&");

        for (String param : params) {
            String[] paramKeyVal = param.split("=");
            this.params.add(new ParameterRequest(
                    paramKeyVal[0],
                    paramKeyVal[1]
            ));
        }
    }

    void addParameter(String nameParam, String value) {
        this.params.add(new ParameterRequest(
                nameParam,
                castTo(value)
        ));
    }

    private Object castTo(String value) {
        if (isNumeric(value)) {
            return Integer.parseInt(value);
        }
        return value;
    }

    private boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");
    }

    public <T> T getBodyAs(Class<T> clazz) {
        if (body == null) {
            return null;
        }

        try {
            return mapper.readValue(
                    body,
                    clazz
            );
        } catch (JsonProcessingException e) {
            throw new HttpException("cannot convert body corps to class %s".formatted(clazz.getName()), e);
        }
    }
}
