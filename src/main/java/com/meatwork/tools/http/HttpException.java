package com.meatwork.tools.http;

import lombok.Getter;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@Getter
class HttpException extends RuntimeException{

    private final HttpStatus httpStatus;


    public HttpException(String message,
                         Throwable cause) {
        this(
                message,
                cause,
                HttpStatus.NOT_FOUND
        );
    }

    public HttpException(String message,
                         Throwable cause, HttpStatus httpStatus) {
        super(
                message,
                cause
        );
        this.httpStatus = httpStatus;
    }

    public HttpException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }
}
