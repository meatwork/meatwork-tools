package com.meatwork.tools.http;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public final class HttpResponse {

    final Object object;

    final HttpStatus httpStatus;


    public HttpResponse(Object object, HttpStatus httpStatus) {
        this.object = object;
        this.httpStatus = httpStatus;
    }

    public static HttpResponse ok() {
        return ok("");
    }

    public static HttpResponse ok(Object o) {
        return new HttpResponse(
                o,
                HttpStatus.OK
        );
    }

}
