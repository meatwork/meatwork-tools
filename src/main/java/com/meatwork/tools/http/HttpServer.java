package com.meatwork.tools.http;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class HttpServer {

    private final Logger logger = LoggerFactory.getLogger(HttpServer.class);

    private final com.sun.net.httpserver.HttpServer httpServer;
    private final int port;

    public HttpServer(int port) {
        try {
            this.port = port;
            httpServer = com.sun.net.httpserver.HttpServer.create(new InetSocketAddress(
                    "localhost",
                    port
            ), 0);
            httpServer.createContext(
                    "/",
                    new HttpHandler()
            );
            httpServer.setExecutor(Executors.newFixedThreadPool(10));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void start() {
        logger.info("Server started on port %s".formatted(port));
        httpServer.start();
    }

    public void stop() {
        logger.info("Server stop");
        httpServer.stop(0);
    }
}
