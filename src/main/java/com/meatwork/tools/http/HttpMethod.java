package com.meatwork.tools.http;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@FunctionalInterface
public interface HttpMethod {

    HttpResponse accept(HttpRequest request);

}
