package com.meatwork.tools.http;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
enum HttpMethodType {
    GET,
    POST,
    PUT,
    PATCH,
    DELETE

}
