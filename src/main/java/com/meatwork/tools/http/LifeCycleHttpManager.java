package com.meatwork.tools.http;

import java.util.HashMap;
import java.util.Map;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class LifeCycleHttpManager {

    private static final Map<HttpComposeKey, HttpMethod> httpList = new HashMap<>();

    private LifeCycleHttpManager() {}

    static void registerRoute(HttpRegistration httpRegistration) {
        httpList.put(httpRegistration.composeKey(), httpRegistration.method());
    }

    public static HttpMethod get(HttpRequest route) {
        return getByRoute(route);
    }

    private static HttpMethod getByRoute(HttpRequest httpRequest) {
        String route = httpRequest.getUrl();
        String method = httpRequest.getHttpMethodType().name();

        for (Map.Entry<HttpComposeKey, HttpMethod> entry : httpList.entrySet()) {
            HttpComposeKey composeKey = entry.getKey();
            String routeCurrent = composeKey.route;

            if (routeCurrent.equals(route) && composeKey.method.equalsIgnoreCase(method)) {
                return entry.getValue();
            }

            if (matches(routeCurrent, route, httpRequest)) {
                return entry.getValue();
            }
        }

        return null;
    }

    private static boolean matches(String savedRoute, String userEnteredUrl, HttpRequest httpRequest) {
        String[] savedParts = savedRoute.split("/");
        String[] userParts = userEnteredUrl.split("/");

        if (savedParts.length != userParts.length) {
            return false;
        }

        for (int i = 0; i < savedParts.length; i++) {
            String savedPart = savedParts[i];
            String userPart = userParts[i];
            if (hasParameter(savedPart)) {
                httpRequest.addParameter(savedPart.replace("[", "").replace("]", ""), userPart);
                continue;
            }

            if (!savedPart.equals(userPart)) {
                return false;
            }
        }

        return true;
    }

    private static boolean hasParameter(String part) {
        return part.startsWith("[") && part.endsWith("]");
    }

    public record HttpComposeKey(String route, String method) {}
}
