package com.meatwork.tools.http;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class HttpManager {

	static <T> HttpMethod get(HttpRequest httpRequest) {
		return LifeCycleHttpManager.get(httpRequest);
	}

	public static <T> void register(String router,
	                                String method,
	                                HttpMethod httpMethod) {
		LifeCycleHttpManager.registerRoute(new HttpRegistration(
				new LifeCycleHttpManager.HttpComposeKey(
						router,
						method
				),
				httpMethod
		));
	}

}
