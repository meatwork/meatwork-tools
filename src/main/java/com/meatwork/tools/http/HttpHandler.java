package com.meatwork.tools.http;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.meatwork.tools.api.http.Authentication;
import com.meatwork.tools.service.ServiceManager;
import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
final class HttpHandler implements com.sun.net.httpserver.HttpHandler {

	private static final ObjectMapper mapper = new ObjectMapper();

	public HttpHandler() {
		mapper.registerModule(new JavaTimeModule());
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
	}

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		try {
			Authentication authentication = ServiceManager.get(Authentication.class);
			HttpRequest httpRequest = new HttpRequest(exchange);
			if (authentication != null && !authentication.checkAuthentication(httpRequest)) {
				throw new HttpException("unauthorized", HttpStatus.UNAUTHORIZED);
			}
			HttpResponse response = HttpManager
					.get(httpRequest)
					.accept(httpRequest);
			createResponse(
					exchange,
					response.object,
					response.httpStatus.getCode()
			);
		} catch (HttpException e) {
			createResponse(exchange, e.getMessage(), e.getHttpStatus().getCode());
		}
	}

	private void createResponse(HttpExchange exchange, Object message, int code) throws IOException {
		OutputStream responseBody = exchange.getResponseBody();
		String json = toJson(message);
		exchange.sendResponseHeaders(
				code,
				json.length()
		);
		exchange.getResponseHeaders().add("Content-Type", "application/json");
		responseBody.write(json.getBytes(StandardCharsets.UTF_8));
		responseBody.flush();
		responseBody.close();
	}

	public String toJson(Object object) {
		try {
			if (object instanceof String && ((String) object).isEmpty()) {
				return "";
			}
			return mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			throw new HttpException("cannot convert %s to json".formatted(object.getClass().getName()), e);
		}
	}
}
