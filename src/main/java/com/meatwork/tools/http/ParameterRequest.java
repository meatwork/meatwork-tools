package com.meatwork.tools.http;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public record ParameterRequest(String key, Object value) {}
