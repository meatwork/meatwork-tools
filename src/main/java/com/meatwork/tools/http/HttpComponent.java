package com.meatwork.tools.http;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public interface HttpComponent {

	void registerRouter();

}
