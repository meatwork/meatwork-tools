package com.meatwork.tools.configuration;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
@RequiredArgsConstructor
@Getter
public enum MeatworkConfigurationType {

	ENABLE_HTTP_SERVER(DEFAULT.ENABLE_HTTP_SERVER_NAME, DEFAULT.ENABLE_HTTP_SERVER_DEFAULT),
	PORT_HTTP_SERVER(DEFAULT.HTTP_PORT_NAME, DEFAULT.HTTP_PORT_DEFAULT),
	ENABLE_ENTITY(DEFAULT.ENABLE_ENTITY_NAME, DEFAULT.ENABLE_ENTITY_DEFAULT),

	;

	@NoArgsConstructor(access = AccessLevel.PRIVATE)
	public static class DEFAULT {
		public static final String HTTP_PORT_NAME = "portHttpServer";
		public static final int HTTP_PORT_DEFAULT = 8080;
		public static final String ENABLE_HTTP_SERVER_NAME = "enableHttpServer";
		public static final boolean ENABLE_HTTP_SERVER_DEFAULT = false;
		public static final boolean ENABLE_ENTITY_DEFAULT = false;
		public static final String ENABLE_ENTITY_NAME = "enableEntity";
	}

	private final String name;
	private final Object value;

	public static MeatworkConfigurationType valueOfName(String name) {
		return switch (name) {
			case DEFAULT.ENABLE_HTTP_SERVER_NAME -> MeatworkConfigurationType.ENABLE_HTTP_SERVER;
			case DEFAULT.HTTP_PORT_NAME -> MeatworkConfigurationType.PORT_HTTP_SERVER;
			case DEFAULT.ENABLE_ENTITY_NAME -> MeatworkConfigurationType.ENABLE_ENTITY;
			default -> throw new IllegalStateException("Unexpected value: " + name);
		};
	}

}
