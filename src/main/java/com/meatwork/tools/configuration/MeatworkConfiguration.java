package com.meatwork.tools.configuration;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MeatworkConfiguration {
	boolean enableHttpServer() default MeatworkConfigurationType.DEFAULT.ENABLE_HTTP_SERVER_DEFAULT;
	int portHttpServer() default MeatworkConfigurationType.DEFAULT.HTTP_PORT_DEFAULT;
	boolean enableEntity() default MeatworkConfigurationType.DEFAULT.ENABLE_ENTITY_DEFAULT;
}
