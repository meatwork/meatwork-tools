/*
 * Copyright (c) 2016 Bull/Atos.
 * All rights reserved.
 */
package com.meatwork.tools.event;

/**
 * [Enter type description here].
 *
 * @author Taliro
 */
public final class StartupRegistration {

    private final Runnable executeMethod;
    private final Integer priority;

    public StartupRegistration(Runnable executeMethod, Integer priority) {
        this.executeMethod = executeMethod;
        this.priority = priority;
    }

    public Runnable getExecuteMethod() {
        return executeMethod;
    }

    public Integer getPriority() {
        return priority;
    }
}
