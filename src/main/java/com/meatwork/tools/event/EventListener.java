/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
package com.meatwork.tools.event;

import java.util.function.Consumer;

/**
 * [Enter type description here].
 *
 * @author Bull/Atos
 */
@FunctionalInterface
public interface EventListener<T> extends Consumer<T> {}
