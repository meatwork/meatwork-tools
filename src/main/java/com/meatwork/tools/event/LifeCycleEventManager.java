/*
 * Copyright (c) 2016 Bull/Atos.
 * All rights reserved.
 */
package com.meatwork.tools.event;


import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * [Enter type description here].
 *
 * @author Bull/Atos
 */
final class LifeCycleEventManager {

    private static final Map<String, EventConfig> eventList = new ConcurrentHashMap<>();

    private LifeCycleEventManager() {}

    static EventConfig getEvent(Class<?> key) {
        return eventList.get(key.getName().replaceAll("\\.", "/"));
    }

    static void registerEvent(EventRegistration<?> eventRegistration) {
        if (eventList.containsKey(eventRegistration.getKey())) {
            EventConfig eventConfig = eventList.get(eventRegistration.getKey());
            eventConfig.eventList.put(eventRegistration.getPriority(), eventRegistration.getEventListener());
        } else {
            TreeMap<Integer, EventListener<?>> value = new TreeMap<>();
            value.put(eventRegistration.getPriority(), eventRegistration.getEventListener());
            eventList.put(eventRegistration.getKey(), new EventConfig(eventRegistration.isSynchronizer(), value));
        }
    }

    record EventConfig(boolean isSynchronizer, Map<Integer, EventListener<?>> eventList) {}
}
