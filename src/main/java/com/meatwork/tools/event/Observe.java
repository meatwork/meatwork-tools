/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
package com.meatwork.tools.event;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * [Enter type description here].
 *
 * @author Bull/Atos
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Observe {
    int priority() default 100;
}
