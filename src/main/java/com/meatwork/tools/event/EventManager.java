package com.meatwork.tools.event;

public final class EventManager {

    private EventManager() {
    }

    static <T> LifeCycleEventManager.EventConfig get(Class<T> clazz) {
        return LifeCycleEventManager.getEvent(clazz);
    }

    public static <T> void register(Class<T> clazz, EventListener<T> eventEvent, Integer priority) {
        LifeCycleEventManager.registerEvent(new EventRegistration<>(clazz, eventEvent, priority, clazz.isAnnotationPresent(Synchronize.class)));
    }

}
