package com.meatwork.tools.event;

import java.util.Map;
import java.util.TreeMap;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
final class LifeCycleStartupManager {

    private static final Map<Integer, Runnable> startupList = new TreeMap<>();

    private LifeCycleStartupManager() {}

    static void registerEvent(StartupRegistration startupRegistration) {
        startupList.put(startupRegistration.getPriority(), startupRegistration.getExecuteMethod());
    }

    static void execute() {
        startupList
                .values().forEach(Runnable::run);
        startupList.clear();
    }

}
