/*
 * Copyright (c) 2016 Bull/Atos.
 * All rights reserved.
 */
package com.meatwork.tools.event;

import com.meatwork.tools.service.ServiceManager;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * [Enter type description here].
 *
 * @author Bull/Atos
 */

public final class BroadcastEvent {

    private static final ExecutorService executorService = Executors.newFixedThreadPool(4);

    private BroadcastEvent() {
    }

    @SuppressWarnings("unchecked")
    public static <T extends Event> void send(T obj) {
        LifeCycleEventManager.EventConfig eventConfig = EventManager.get(obj.getClass());

        if (eventConfig == null) {
            return;
        }

        if (eventConfig.isSynchronizer() || ServiceManager.get(TestModeEnable.class) != null) {
            eventConfig.eventList().values().forEach(it -> ((EventListener<T>) it).accept(obj));
        } else {
            executorService.submit(() ->
                    eventConfig.eventList().values().forEach(it -> ((EventListener<T>) it).accept(obj))
            );
        }
    }
}
