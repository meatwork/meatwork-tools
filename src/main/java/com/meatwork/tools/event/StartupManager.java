package com.meatwork.tools.event;

/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
public class StartupManager {

    public static void register(Runnable runnable, Integer priority) {
        LifeCycleStartupManager.registerEvent(new StartupRegistration(runnable, priority));
    }

    public static void execute() { LifeCycleStartupManager.execute(); }

}
