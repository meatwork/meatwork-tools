/*
 * Copyright (c) 2016 Bull/Atos.
 * All rights reserved.
 */
package com.meatwork.tools.event;

import lombok.Getter;

/**
 * [Enter type description here].
 *
 * @author Taliro
 */
@Getter
public final class EventRegistration<T> {

    private final String key;
    private final EventListener<?> eventListener;
    private final Integer priority;
    private final boolean synchronizer;

    public EventRegistration(Class<?> clazz, EventListener<?> event, Integer priority, boolean synchronizer) {
        this.key = clazz.getName().replaceAll("\\.", "/");
        this.eventListener = event;
        this.priority = priority;
        this.synchronizer = synchronizer;
    }
}
