/*
 * Copyright (c) 2016 Taliro.
 * All rights reserved.
 */
package com.meatwork.tools.event;

/**
 * Event component interface
 *
 */
public interface EventComponent {

    /**
     * Register event.
     */
    void registerEvent();

    /**
     * Register startup.
     */
    void registerStartup();

    /**
     * Register service.
     */
    void registerService();

    /**
     * Register bind.
     */
    void registerBind();

}
